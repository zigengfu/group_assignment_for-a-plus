/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package assignment_4;

import assignment_4.entities.Product;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author harshalneelkamal
 */
public class GateWay {   
    public static void main(String args[]) throws IOException{
        top3product();
        top3customer();
        top3saleman();
        totalrevenue();
    }
    
//    public static void printRow(String[] row){
//        for (String row1 : row) {
//            System.out.print(row1 + ", ");
//        }
//        System.out.println("");
//    }
    public static void top3product() throws IOException{
        DataGenerator generator = DataGenerator.getInstance();       
        DataReader productReader = new DataReader(generator.getOrderFilePath());
        String[] prodRow;
        HashMap<Integer,Integer> mostPopProduct = new HashMap<>();
        
        while((prodRow = productReader.getNextRow()) != null){
            int quantity=Integer.parseInt(prodRow[3]);
            int prodID=Integer.parseInt(prodRow[2]);
            if(mostPopProduct.containsKey(prodID))
                quantity += mostPopProduct.get(prodID);
            mostPopProduct.put(prodID, quantity);            
        }                                           
        List<Integer> prodQuantity = new ArrayList<>(mostPopProduct.values());
        Collections.sort(prodQuantity, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i2 - i1;
            }
        });       
        int[] countNum = new int[3];
        int j=0;
        for(int i=0;i<prodQuantity.size() && j<3;i++){
            if(i==0 || prodQuantity.get(i)!=countNum[j])
                countNum[j++] = prodQuantity.get(i);
        }
        
        int[] prodID = new int[3];
        for(int id  : mostPopProduct.keySet()){
            if(mostPopProduct.get(id)==countNum[0]) prodID[0]=id;
            else if(mostPopProduct.get(id)==countNum[1]) prodID[1]=id;
            else if(mostPopProduct.get(id)==countNum[2]) prodID[2]=id;
        }
        
        System.out.println("Top 3 most popular product: "); 
        for(int i=0;i<3;i++)
            System.out.println("ProductID: "+prodID[i]+", "+"Quantity: "+countNum[i]);
        System.out.println();  
    }
    
    
    public static void top3customer() throws IOException{
        DataGenerator generator = DataGenerator.getInstance();       
        DataReader productReader = new DataReader(generator.getOrderFilePath());
        String[] prodRow;
        HashMap<Integer,Integer> top3customer = new HashMap<>();
        while((prodRow = productReader.getNextRow()) != null){
            int money=Integer.parseInt(prodRow[3])*Integer.parseInt(prodRow[6]);
            int customerID=Integer.parseInt(prodRow[5]);
            if(top3customer.containsKey(customerID))
                money += top3customer.get(customerID);
            top3customer.put(customerID, money);            
        }                                           
        List<Integer> money = new ArrayList<>(top3customer.values());
        Collections.sort(money, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i2 - i1;
            }
        });  
        int[] top3money = new int[3];
        int j=0;
        for(int i=0;i<money.size() && j<3;i++){
            if(i==0 || money.get(i)!=top3money[j])
                top3money[j++] = money.get(i);
        }
        
        int[] customerID = new int[3];
        for(int id  : top3customer.keySet()){
            if(top3customer.get(id)==top3money[0]) customerID[0]=id;
            else if(top3customer.get(id)==top3money[1]) customerID[1]=id;
            else if(top3customer.get(id)==top3money[2]) customerID[2]=id;
        }
        
        System.out.println("Top 3 best customer: "); 
        for(int i=0;i<3;i++)
            System.out.println("CustomerID: "+customerID[i]+", "+"Cost: "+top3money[i]);
        System.out.println();        
    }
    
    public static void top3saleman() throws IOException{
        DataGenerator generator = DataGenerator.getInstance();       
        DataReader productReader = new DataReader(generator.getOrderFilePath());
        String[] prodRow;
        HashMap<Integer,Integer> top3saleman = new HashMap<>();
        while((prodRow = productReader.getNextRow()) != null){
            int money=Integer.parseInt(prodRow[3])*Integer.parseInt(prodRow[6]);
            int salemanID=Integer.parseInt(prodRow[4]);
            if(top3saleman.containsKey(salemanID))
                money += top3saleman.get(salemanID);
            top3saleman.put(salemanID, money);            
        }                                           
        List<Integer> money = new ArrayList<>(top3saleman.values());
        Collections.sort(money, new Comparator<Integer>() {
            @Override
            public int compare(Integer i1, Integer i2) {
                return i2 - i1;
            }
        }); 
        int[] top3money = new int[3];
        int j=0;
        for(int i=0;i<money.size() && j<3;i++){
            if(i==0 || money.get(i)!=top3money[j])
                top3money[j++] = money.get(i);
        }
        
        int[] salemanID = new int[3];
        for(int id  : top3saleman.keySet()){
            if(top3saleman.get(id)==top3money[0]) salemanID[0]=id;
            else if(top3saleman.get(id)==top3money[1]) salemanID[1]=id;
            else if(top3saleman.get(id)==top3money[2]) salemanID[2]=id;
        }
        
        System.out.println("Top 3 best saleman: "); 
        for(int i=0;i<3;i++)
            System.out.println("salemanID: "+salemanID[i]+", "+"Cost: "+top3money[i]);
        System.out.println();  
        
    }
    
    public static void totalrevenue() throws IOException{
        int totalRevenue=0;
        DataGenerator generator = DataGenerator.getInstance();       
        DataReader productReader = new DataReader(generator.getOrderFilePath());
        DataReader minpriceReader = new DataReader(generator.getProductCataloguePath());
        String[] prodRow; String[] minpriceRow;
        
        HashMap<Integer,Integer> minprice = new HashMap<>();
        while((minpriceRow = minpriceReader.getNextRow()) != null){
            minprice.put(Integer.parseInt(minpriceRow[0]), Integer.parseInt(minpriceRow[1]));
        }
        
//        HashMap<Integer,Integer> saleprice = new HashMap<>();
        while((prodRow = productReader.getNextRow()) != null){
            int priceDiff = Integer.parseInt(prodRow[6]) - minprice.get(Integer.parseInt(prodRow[2]));
            int quantity = Integer.parseInt(prodRow[3]);
            int moneyPerOrder = quantity * priceDiff;
            
            totalRevenue += moneyPerOrder;
        }                                           
        
        System.out.println("Total Revenue For the Year: "+totalRevenue); 
    }
}
