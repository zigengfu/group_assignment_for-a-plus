/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Organization.Organization;
import Business.Region.Region;
import java.util.ArrayList;

/**
 *
 * @author lx
 */
public class MainSystem extends Organization{
    private static MainSystem system;
    private ArrayList<Region> regionList;
    
    public static MainSystem getInstance(){
        if(system == null){
            system = new MainSystem();
        }       
        return system;
    }
    
    public Region createRegion(){
        Region region = new Region();
        regionList.add(region);
        return region;
    }
    //public ArrayList<>
            
    private MainSystem(){
        super(null);
        regionList = new ArrayList<Region>();
    }
            
    public ArrayList<Region> getRegionList(){
        return regionList;
    }
    
    public void setRegionList(ArrayList<Region> networkList) {
        this.regionList = regionList;
    }
    
    public boolean checkIfUserIsUnique(String userName){
        if(!this.checkIfUserIsUnique(userName)){
            return false;
        }
//        for(Network network:networkList){
//            
//        }
        return true;
    }
}
