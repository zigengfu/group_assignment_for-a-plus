/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author lx
 */
public class OrganizationDirectory {
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }
    
    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(String name,Organization.OrganizationType type){
        Organization organization  = null;      
        if (type.getTypeString().equals(Organization.OrganizationType.SupplyDepartment.getTypeString())){
            organization = new SupplyDepartment(name);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.ProductionDepartment.getTypeString())){
            organization = new ProductionDepartment(name);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.MarketingDepartment.getTypeString())){
            organization = new MarketingDepartment(name);
            organizationList.add(organization);
        }
        
        return organization;
    }
}
