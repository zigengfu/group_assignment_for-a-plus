/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Enterprise.Enterprise;
import Business.UserAccount.UserAccountDirectory;

/**
 *
 * @author lx
 */
public abstract class Organization{

    private String name;
    private int organizationID;
    private static int counter = 0;
    private OrganizationType organizationType;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;

   
    public Organization(String name){
        this.name = name;
        this.organizationType = organizationType;      
        organizationID = counter++;
        employeeDirectory = new EmployeeDirectory();       
        userAccountDirectory = new UserAccountDirectory();
        
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
    }
    
    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public int getOrganizationID() {
        return organizationID;
    }
  
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public enum OrganizationType{
        SupplyDepartment("SupplyDepartment"),
        ProductionDepartment("ProductionDepartment"),
        MarketingDepartment("MarketingDepartment");
        
        private String typeString;
        private OrganizationType(String typeString) {
            this.typeString = typeString;
        }
        public String getTypeString() {
            return typeString;
        }        
        @Override
        public String toString(){
            return typeString;
        }
    }
    
    @Override
    public String toString() {
        return name;
    } 
}
