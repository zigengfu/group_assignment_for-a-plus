/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.MainSystem;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.SystemAdmin.SystemAdminJPanel;
import javax.swing.JPanel;

/**
 *
 * @author lx
 */
public class SystemAdminRole extends Role{
    @Override
    public JPanel createWorkerArea(JPanel jPanel, UserAccount userAccount, Organization organization, Enterprise enterprise, MainSystem system) {
        return new SystemAdminJPanel(jPanel,system);
    }
}
