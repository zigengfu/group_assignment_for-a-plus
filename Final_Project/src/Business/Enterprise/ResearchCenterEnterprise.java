/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Enterprise.Enterprise.EnterpriseType;

/**
 *
 * @author lx
 */
public class ResearchCenterEnterprise extends Enterprise {
    public ResearchCenterEnterprise(String name){
        super(name,EnterpriseType.ResearchCenter);
    }
}
