/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import Business.Employee.Employee;
/**
 *
 * @author lx
 */
public class ConfigureSystem {
    public static MainSystem configure(){
        MainSystem system = MainSystem.getInstance();
        
        //Employee employee = system.;
        UserAccount user = system.getUserAccountDirectory().createUserAccount("admin", "admin", new Employee(), new SystemAdminRole());
        return system;
    }
}
