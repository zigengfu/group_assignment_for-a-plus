/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.FactoryRoles;

import Business.Enterprise.Enterprise;
import Business.Object.Formulation;
import Business.Organization.FactoryProductionDepartment;
import Business.Organization.Organization;
import Business.Region.Region;
import Business.Role.Factory.FactoryWorkerRole;
import Business.Role.Role;
import Business.Role.Role.RoleType;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.Factory.FactoryInnerReqDirectory;
import Business.WorkQueue.Factory.FactoryInnerRequest;
import Business.WorkQueue.ResearchCenter.FormulationRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Component;
import java.awt.Image;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author lx
 */
public class TaskJPanel extends javax.swing.JPanel {
    private JPanel jPanel;
    private UserAccount userAccount;
    private FactoryProductionDepartment factoryProductionDepartment;
    private Enterprise enterprise;
//    FactoryInnerReqDirectory factoryInnerReqDirectory;
    /**
     * Creates new form WorkerJPanel
     */

    public TaskJPanel(JPanel jPanel, UserAccount userAccount, FactoryProductionDepartment factoryProductionDepartment, Enterprise enterprise) {
        initComponents();
        this.jPanel = jPanel;
        this.userAccount = userAccount;
        this.factoryProductionDepartment = factoryProductionDepartment;
        this.enterprise = enterprise;
        valueLabel.setText(factoryProductionDepartment.getOrganizationType().getTypeString());
//        factoryInnerReqDirectory = new FactoryInnerReqDirectory();
                String pathIconFinish = "src/images/back.png";
         String pathIconAdd = "src/images/add.png";
         String pathIconSend="src/images/send.png";
         String pathIconBack="src/images/back.png";
        ImageIcon iconLogout = new ImageIcon(pathIconFinish);
         ImageIcon iconAdd = new ImageIcon(pathIconAdd);
         ImageIcon iconSend = new ImageIcon(pathIconSend);
          ImageIcon iconProcess = new ImageIcon(pathIconBack);
        
        iconLogout.setImage(iconLogout.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconAdd.setImage(iconAdd.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconSend.setImage(iconSend.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconProcess.setImage(iconProcess.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        this.btnAssign.setIcon(iconAdd);
        this.btnBack.setIcon(iconLogout);
        populateTable();
        populateComboBox();
    }
    
    public void populateTable() {
        DefaultTableModel model = (DefaultTableModel) jTable.getModel();
        model.setRowCount(0);
//        HashMap<String,String> map = null;
        
        for(WorkRequest workRequest : factoryProductionDepartment.getWorkQueue().getWorkRequestList()){
            FactoryInnerRequest innerRequest = (FactoryInnerRequest)workRequest;
//            map = new HashMap<>();
//            map.put(innerRequest.getDrugName(), innerRequest.getStatus());
            
            Object[] row = new Object[2];
            row[0] = innerRequest.getDrugName();
            row[1] = innerRequest.getStatus();
            model.addRow(row); 
        }
        
//        for(String str : map.keySet()){
//            Object[] row = new Object[2];
//            row[0] = str;
//            row[1] = map.get(str);
//            model.addRow(row); 
//        }
       
    }
    
    private void populateComboBox() {
        drugjComboBox.removeAllItems();
        WorkerjComboBox.removeAllItems();
        HashMap<String,String> map = null;
        
        for(WorkRequest workRequest : factoryProductionDepartment.getWorkQueue().getWorkRequestList()){
            FactoryInnerRequest innerRequest = (FactoryInnerRequest)workRequest;
            drugjComboBox.addItem(innerRequest.getDrugName());
        }
        
//        for(String str : map.keySet()){
//             drugjComboBox.addItem(str);
//        }
//        for(WorkRequest workRequest : factoryProductionDepartment.getWorkQueue().getWorkRequestList()){
//            FactoryInnerRequest innerRequest = (FactoryInnerRequest)workRequest;
//            drugjComboBox.addItem(innerRequest.getDisease()); 
//            
//        }
        
        for(UserAccount userAccount : factoryProductionDepartment.getUserAccountDirectory().getUserAccountList()){
            if(userAccount.getRole().toString().equals(factoryProductionDepartment.getSupportedRole().get(1).toString())){
                WorkerjComboBox.addItem(userAccount);
            }
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        btnAssign = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        drugjComboBox = new javax.swing.JComboBox();
        WorkerjComboBox = new javax.swing.JComboBox();
        btnBack = new javax.swing.JButton();

        enterpriseLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        enterpriseLabel.setText("Organization :");

        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Drug", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(jTable);

        btnAssign.setText("Assign");
        btnAssign.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAssignActionPerformed(evt);
            }
        });

        jLabel1.setText("Drug");

        jLabel2.setText("Worker");

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAssign))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(54, 54, 54)
                                .addComponent(enterpriseLabel)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(14, 14, 14)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 372, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 8, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(WorkerjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(drugjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(drugjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(WorkerjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnAssign)
                    .addComponent(btnBack))
                .addContainerGap(30, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnAssignActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAssignActionPerformed
        // TODO add your handling code here:

        for(WorkRequest workRequest : factoryProductionDepartment.getWorkQueue().getWorkRequestList()){
            FactoryInnerRequest innerRequest = (FactoryInnerRequest)workRequest;
            if(innerRequest.getDrugName().equals(drugjComboBox.getSelectedItem().toString())){              
                for(UserAccount user : factoryProductionDepartment.getUserAccountDirectory().getUserAccountList()){
                    if(user.getUsername().equals(WorkerjComboBox.getSelectedItem().toString())){ 
                        if(innerRequest.getStatus()!=null && innerRequest.getStatus().equals("Pending")){
                        user.getWorkQueue().getWorkRequestList().add(innerRequest);
                        innerRequest.setStatus("Sent");
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "This task has been sent!");
                            return;
                        }
                    }
                }
                
            }
            
        } 
        
        for(Organization o : enterprise.getOrganizationDirectory().getOrganizationList()){
            if(o.getOrganizationType().toString().equals(Organization.OrganizationType.SupplyDepartment.getTypeString())){
                for(WorkRequest workRequest : o.getWorkQueue().getWorkRequestList()){
                    FactoryInnerRequest innerRequest = (FactoryInnerRequest)workRequest;
                    if(innerRequest.getDrugName().equals(drugjComboBox.getSelectedItem().toString())){
                        innerRequest.setStatus("Sent");
                    }
 //                    innerRequest.setStatus("Pending");
//                    this.factoryProductionDepartment.getWorkQueue().getWorkRequestList().add(innerRequest);
                }
            }
        }
        
        populateTable();
    }//GEN-LAST:event_btnAssignActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        jPanel.remove(this);
        Component[] componentArray = jPanel.getComponents();
        Component component = componentArray[componentArray.length - 1];
        ProductionDepartmentJPanel productionDepartmentJPanel = (ProductionDepartmentJPanel)component;
        productionDepartmentJPanel.populateTable();
        CardLayout layout = (CardLayout)jPanel.getLayout();
        layout.previous(jPanel);
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox WorkerjComboBox;
    private javax.swing.JButton btnAssign;
    private javax.swing.JButton btnBack;
    private javax.swing.JComboBox drugjComboBox;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable;
    private javax.swing.JLabel valueLabel;
    // End of variables declaration//GEN-END:variables
}
