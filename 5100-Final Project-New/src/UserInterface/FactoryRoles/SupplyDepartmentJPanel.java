/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.FactoryRoles;

import Business.Enterprise.Enterprise;
import Business.Object.Formulation;
import Business.Organization.Organization;
import Business.Organization.Organization.OrganizationType;
import Business.Organization.FactorySupplyDepartment;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.Factory.FactoryInnerReqDirectory;
import Business.WorkQueue.Factory.FactoryInnerRequest;
import Business.WorkQueue.ResearchCenter.FormulationRequest;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkRequest;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author lx
 */
public class SupplyDepartmentJPanel extends javax.swing.JPanel {
    private JPanel jPanel;
    private UserAccount userAccount;
    private Organization organization;
    private Enterprise enterprise;
    FactoryInnerReqDirectory factoryInnerReqDirectory;
    /**
     * Creates new form PurchaserJPanel
     */

    public SupplyDepartmentJPanel(JPanel jPanel, UserAccount userAccount, Organization organization, Enterprise enterprise) {
        initComponents();
        String pathIconBack = "src/images/back.png";
         String pathIconView = "src/images/view.png";
         String pathIconSend="src/images/confirm.png";
         String pathIconProcess="src/images/process.png";
         ImageIcon iconBack = new ImageIcon(pathIconBack);
         ImageIcon iconView = new ImageIcon(pathIconView);
         ImageIcon iconSend = new ImageIcon(pathIconSend);
          ImageIcon iconProcess = new ImageIcon(pathIconProcess);
        
        iconBack.setImage(iconBack.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconView.setImage(iconView.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconSend.setImage(iconSend.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconProcess.setImage(iconProcess.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        this.btnSubmit.setIcon(iconSend);
        this.jPanel = jPanel;
        this.userAccount = userAccount;
        this.organization = (FactorySupplyDepartment)organization;
        this.enterprise = enterprise;
        factoryInnerReqDirectory = new FactoryInnerReqDirectory();
        valueLabel.setText(OrganizationType.SupplyDepartment.getTypeString());
        System.out.println("****"+enterprise.getName());
        
        this.organization.getWorkQueue().getWorkRequestList().clear();
        
        for(WorkRequest workRequest : userAccount.getWorkQueue().getWorkRequestList()){
            FormulationRequest formulationRequest = (FormulationRequest)workRequest;
            FactoryInnerRequest innerRequest =null;
            for(Formulation f : formulationRequest.getFormulation()){  
                String disease = formulationRequest.getDisease().getDiseaseName();
                String drugName = "Drug for " + disease;
                innerRequest = new FactoryInnerRequest(drugName,disease,f.getMaterialQuantity(),f.getMaterialStock());
                                 
            }
            factoryInnerReqDirectory.getFactoryInnerReqDirectory().add(innerRequest);               
        }
        
//        userAccount.getWorkQueue().getWorkRequestList().clear();
        for(FactoryInnerRequest innerRequest : factoryInnerReqDirectory.getFactoryInnerReqDirectory()){
            organization.getWorkQueue().getWorkRequestList().add(innerRequest);
         
         }
        populateTable();
        populateCombo();
    }

    private void populateTable() {
        DefaultTableModel model = (DefaultTableModel) materialjTable.getModel();
        model.setRowCount(0);
     
        for(FactoryInnerRequest innerRequest : factoryInnerReqDirectory.getFactoryInnerReqDirectory()){
            for(String str : innerRequest.getMaterialQuantity().keySet()){
                Object[] row = new Object[5];         
                row[0] = innerRequest.getDrugName();
                row[1] = str;
                row[2] = innerRequest.getMaterialQuantity().get(str);
                row[3] = innerRequest.getDisease();
                row[4] = innerRequest.getMaterialStock().get(str);
                model.addRow(row);
            }            
        }
    }
    
    private void populateCombo(){      
        drugNamejComboBox.removeAllItems();
        quantityjComboBox.removeAllItems();
        
        for(FactoryInnerRequest innerRequest : factoryInnerReqDirectory.getFactoryInnerReqDirectory()){
                drugNamejComboBox.addItem(innerRequest.getDrugName());                          
        }
      
        for(int i=0;i<=100;i++){
            quantityjComboBox.addItem(i);
        }
    }

    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        materialjTable = new javax.swing.JTable();
        enterpriseLabel = new javax.swing.JLabel();
        valueLabel = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        drugNamejComboBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        ingredientjComboBox = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        quantityjComboBox = new javax.swing.JComboBox();
        btnSubmit = new javax.swing.JButton();

        materialjTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Drug", "Ingredient", "Quantity", "Disease", "Stock"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane1.setViewportView(materialjTable);

        enterpriseLabel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        enterpriseLabel.setText("Organization :");

        jLabel1.setText("Drug");

        drugNamejComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                drugNamejComboBoxActionPerformed(evt);
            }
        });

        jLabel2.setText("Ingredient");

        ingredientjComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ingredientjComboBoxActionPerformed(evt);
            }
        });

        jLabel3.setText("Quantity");

        btnSubmit.setText("Submit");
        btnSubmit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSubmitActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnSubmit)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGap(44, 44, 44)
                            .addComponent(enterpriseLabel)
                            .addGap(18, 18, 18)
                            .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addContainerGap(37, Short.MAX_VALUE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(drugNamejComboBox, 0, 198, Short.MAX_VALUE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel2)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(ingredientjComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(quantityjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 517, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(76, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(58, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(enterpriseLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(valueLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(drugNamejComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(ingredientjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(quantityjComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(btnSubmit)
                .addContainerGap(106, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSubmitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSubmitActionPerformed
        // TODO add your handling code here:
        int temp = Integer.parseInt(quantityjComboBox.getSelectedItem().toString());
        
        for(FactoryInnerRequest innerRequest : factoryInnerReqDirectory.getFactoryInnerReqDirectory()){           
                if(innerRequest.getDrugName().equals(drugNamejComboBox.getSelectedItem().toString())){
                    
                    for(String str : innerRequest.getMaterialQuantity().keySet()){
                        if(str.equals(ingredientjComboBox.getSelectedItem().toString()))
                            if(innerRequest.getMaterialStock().get(str) == null) 
                                innerRequest.getMaterialStock().put(str, temp);
                            else
                                innerRequest.getMaterialStock().put(str, temp+innerRequest.getMaterialStock().get(str));                            
                    }
                }       
        }       
        
        populateTable();
    }//GEN-LAST:event_btnSubmitActionPerformed

    private void ingredientjComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ingredientjComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ingredientjComboBoxActionPerformed

    private void drugNamejComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_drugNamejComboBoxActionPerformed
        // TODO add your handling code here:
        String drugName = this.drugNamejComboBox.getSelectedItem().toString();
        if(drugName!=null){
            populateIngredient(drugName);
        }
    }//GEN-LAST:event_drugNamejComboBoxActionPerformed

    private void populateIngredient(String drugName) {
        ingredientjComboBox.removeAllItems();
        for(FactoryInnerRequest innerRequest : factoryInnerReqDirectory.getFactoryInnerReqDirectory()){
            if(innerRequest.getDrugName().equals(drugName)){
                for(String str : innerRequest.getMaterialQuantity().keySet()){                      
                    ingredientjComboBox.addItem(str);
                }
            }
        }
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSubmit;
    private javax.swing.JComboBox drugNamejComboBox;
    private javax.swing.JLabel enterpriseLabel;
    private javax.swing.JComboBox ingredientjComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable materialjTable;
    private javax.swing.JComboBox quantityjComboBox;
    private javax.swing.JLabel valueLabel;
    // End of variables declaration//GEN-END:variables

}
