/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Hospital.DoctorRole;

import Business.Enterprise.Enterprise;
import Business.Enterprise.Enterprise.EnterpriseType;
import Business.Enterprise.EnterpriseDirectory;
import Business.Enterprise.HospitalEnterprise;
import Business.Enterprise.ResearchEnterprise;
import Business.MainSystem;
import Business.Object.DiseaseSample;
import Business.Role.Role;
import Business.Role.Role.RoleType;
import Business.Object.Patient;
import Business.Organization.Organization;
import Business.Organization.Organization.OrganizationType;
import Business.Organization.OrganizationDirectory;
import Business.Region.Region;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.Hospital.DiseaseSampleRequest;
import Business.WorkQueue.Hospital.PatientTreatmentRequest;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import java.text.SimpleDateFormat;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fuzigeng
 */
public class ManageDiseaseSampleJPanel extends javax.swing.JPanel {
    
    SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
    
    private HospitalEnterprise hospital;
    private JPanel userProcessContainer;
    private UserAccount doctorAccount;
    private Region currentRegion;
    private MainSystem system;
    private EnterpriseDirectory innerResearchCenterDirectory = new EnterpriseDirectory();
    private EnterpriseDirectory exterResearchCenterDirectory = new EnterpriseDirectory();
    
    
    /**
     * Creates new form ManageOrganizationJPanel
     */
    public ManageDiseaseSampleJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, Region region, MainSystem system) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.doctorAccount = userAccount;
        this.hospital = (HospitalEnterprise)enterprise;
        this.currentRegion = region;
        this.system = system;
         String pathIconAdd = "src/images/confirm.png";
         String pathIconBack="src/images/back.png";

         ImageIcon iconAdd= new ImageIcon(pathIconAdd);
         ImageIcon iconBack = new ImageIcon(pathIconBack);
        
        iconAdd.setImage(iconAdd.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconBack.setImage(iconBack.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
                this.backJButton.setIcon(iconBack);
        this.confirmJButton.setIcon(iconAdd);
        
        //Inner-network communication
        //Traverse the enterprise directory of the network get the research center
        for(Enterprise etp : region.getEnterpriseDirectory().getEnterpriseList()){
            if(etp.getEnterpriseType() == EnterpriseType.ResearchCenter){
                this.innerResearchCenterDirectory.getEnterpriseList().add(etp);
            }
        }
        
        //Cross-network communication
        //Traverse the enterprise directory of each network of the system and get a list of research centers
        for(Region rg : system.getRegionList()){
            //If region is not the current region
            if(!rg.equals(currentRegion)){
                for(Enterprise etp : rg.getEnterpriseDirectory().getEnterpriseList()){
                    if(etp.getEnterpriseType() == EnterpriseType.ResearchCenter){
                        this.exterResearchCenterDirectory.getEnterpriseList().add(etp);
                    }
                }
            } 
        }
        
        populateTable();
        populateRequestTypeCombo();
      
    }
    
     private void populateTable(){
        DefaultTableModel model = (DefaultTableModel) diseaseSampleListJTable.getModel();   
        model.setRowCount(0);
        
        for(WorkRequest workRequest : doctorAccount.getWorkQueue().getWorkRequestList()){
            if(workRequest instanceof DiseaseSampleRequest){
                DiseaseSampleRequest req =  (DiseaseSampleRequest)workRequest;
                
                Object[] row = new Object[5];
                row[0] = req;
                row[1] = req.getDs().getDescription();
                row[2] = req.getReceiverEnterprise().getName();
                row[3] = sdf.format(req.getRequestDate());
                row[4] = req.getStatus();

                model.addRow(row);
            }
            
        }
        
    }
    
    private void populateRequestTypeCombo(){
        requestTypeJComboBox.removeAllItems();
        requestTypeJComboBox.addItem("Inner-Region");
        requestTypeJComboBox.addItem("Cross-Region");
    }
    
    private void populateReceiverCombo(String requestType){
        receiverJComboBox.removeAllItems();
        if(requestType.equals("Inner-Region")){
            if(!innerResearchCenterDirectory.getEnterpriseList().isEmpty()){
                for(Enterprise etp : innerResearchCenterDirectory.getEnterpriseList()){
                    receiverJComboBox.addItem(etp);
                }
            } 
            else{
                receiverJComboBox.addItem("No research center");
            }
        }
        else if(requestType.equals("Cross-Region")){
            if(!exterResearchCenterDirectory.getEnterpriseList().isEmpty()){
                for(Enterprise etp : exterResearchCenterDirectory.getEnterpriseList()){
                    receiverJComboBox.addItem(etp);
                }
            }
            else{
                receiverJComboBox.addItem("No research center");
            }
        }
        
    }

   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        jScrollPane = new javax.swing.JScrollPane();
        diseaseSampleListJTable = new javax.swing.JTable();
        confirmJButton = new javax.swing.JButton();
        backJButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        descriptionJTextArea = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        receiverJComboBox = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        diseaseNameJTextField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        requestTypeJComboBox = new javax.swing.JComboBox();

        diseaseSampleListJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Disease Name", "Description", "Receiver", "Request Date", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane.setViewportView(diseaseSampleListJTable);
        if (diseaseSampleListJTable.getColumnModel().getColumnCount() > 0) {
            diseaseSampleListJTable.getColumnModel().getColumn(0).setPreferredWidth(70);
            diseaseSampleListJTable.getColumnModel().getColumn(1).setPreferredWidth(150);
            diseaseSampleListJTable.getColumnModel().getColumn(2).setPreferredWidth(80);
            diseaseSampleListJTable.getColumnModel().getColumn(3).setPreferredWidth(80);
            diseaseSampleListJTable.getColumnModel().getColumn(4).setPreferredWidth(60);
        }

        confirmJButton.setText("Confirm");
        confirmJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmJButtonActionPerformed(evt);
            }
        });

        backJButton.setText(" Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Al Bayan", 1, 16)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Disease Sample List");

        jLabel6.setFont(new java.awt.Font("Al Bayan", 1, 16)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Create Disease Research Reqeust");

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Description:");

        descriptionJTextArea.setColumns(20);
        descriptionJTextArea.setRows(5);
        jScrollPane1.setViewportView(descriptionJTextArea);

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Receiver:");

        receiverJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Disease Name:");

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Request Type:");

        requestTypeJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        requestTypeJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestTypeJComboBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(117, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(diseaseNameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 465, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(confirmJButton))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(requestTypeJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(receiverJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(backJButton)
                            .addGap(425, 425, 425))
                        .addComponent(jScrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 573, Short.MAX_VALUE)
                        .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap(126, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(21, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(37, 37, 37)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(requestTypeJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(receiverJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(diseaseNameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(confirmJButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(backJButton)
                .addGap(66, 66, 66))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void confirmJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmJButtonActionPerformed
        
        //Create new disease sample request (will be sent by doctor)
        //There is no receiver user account for a disease sample request
        ResearchEnterprise receiverEnterprise = (ResearchEnterprise)receiverJComboBox.getSelectedItem();
        
        String diseaseName = diseaseNameJTextField.getText();
        String diseaseDescription = descriptionJTextArea.getText();
        DiseaseSample ds = new DiseaseSample(diseaseName, diseaseDescription);
        
        DiseaseSampleRequest newRequest = new DiseaseSampleRequest();
        newRequest.setDs(ds);
        newRequest.setSender(doctorAccount);
        newRequest.setReceiverEnterprise(receiverEnterprise);
        newRequest.setSenderEnterprise(hospital);
        newRequest.setStatus("Pending");
        
        //Add the work request to receiver enterprise's work queue
        receiverEnterprise.getWorkQueue().getWorkRequestList().add(newRequest);
        //Add the work request to sender's work queue
        doctorAccount.getWorkQueue().getWorkRequestList().add(newRequest);
        
        //Update status of the request received by doctor
        populateTable();
    }//GEN-LAST:event_confirmJButtonActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void requestTypeJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestTypeJComboBoxActionPerformed
        // TODO add your handling code here:
        String requestType = (String) requestTypeJComboBox.getSelectedItem();
        if(requestType != null){
            populateReceiverCombo(requestType);
        }      
    }//GEN-LAST:event_requestTypeJComboBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.JButton confirmJButton;
    private javax.swing.JTextArea descriptionJTextArea;
    private javax.swing.JTextField diseaseNameJTextField;
    private javax.swing.JTable diseaseSampleListJTable;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox receiverJComboBox;
    private javax.swing.JComboBox requestTypeJComboBox;
    // End of variables declaration//GEN-END:variables
}
