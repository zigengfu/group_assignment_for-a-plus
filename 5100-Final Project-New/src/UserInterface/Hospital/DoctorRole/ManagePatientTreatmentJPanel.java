/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Hospital.DoctorRole;

import Business.Enterprise.Enterprise;
import Business.Enterprise.HospitalEnterprise;
import Business.Role.Role;
import Business.Role.Role.RoleType;
import Business.Object.Patient;
import Business.Organization.Organization;
import Business.Organization.Organization.OrganizationType;
import Business.Organization.OrganizationDirectory;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.Hospital.PatientTreatmentRequest;
import Business.WorkQueue.WorkQueue;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fuzigeng
 */
public class ManagePatientTreatmentJPanel extends javax.swing.JPanel {

    private HospitalEnterprise hospital;
    private JPanel userProcessContainer;
    private UserAccount doctorAccount;
    private Organization nurseStation;
    private Organization pharmacy;
    
    /**
     * Creates new form ManageOrganizationJPanel
     */
    public ManagePatientTreatmentJPanel(JPanel userProcessContainer,UserAccount userAccount, Enterprise enterprise) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.doctorAccount = userAccount;
        this.hospital = (HospitalEnterprise)enterprise;
        String pathIconAdd = "src/images/confirm.png";
         String pathIconBack="src/images/back.png";

         ImageIcon iconAdd= new ImageIcon(pathIconAdd);
         ImageIcon iconBack = new ImageIcon(pathIconBack);
        
        iconAdd.setImage(iconAdd.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconBack.setImage(iconBack.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
                this.backJButton.setIcon(iconBack);
        this.confirmJButton.setIcon(iconAdd);
        //Traverse the organization directory of the hospital and get the nurse station
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organization.getOrganizationType() == OrganizationType.NurseStation){
                this.nurseStation = organization;
            }
        }
        
         //Traverse the organization directory of the hospital and get the phamacy
        for(Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
            if (organization.getOrganizationType() == OrganizationType.Pharmacy){
                this.pharmacy = organization;
            }
        }
        
        populateTable();
        populateOperationCombo();
      
    }
    
     private void populateTable(){
        DefaultTableModel model = (DefaultTableModel) patientListJTable.getModel();   
        model.setRowCount(0);
        
        
        for(WorkRequest workRequest : doctorAccount.getWorkQueue().getWorkRequestList()){
            if(workRequest instanceof PatientTreatmentRequest){
                PatientTreatmentRequest req =  (PatientTreatmentRequest)workRequest;
                Object[] row = new Object[5];
                row[0] = req.getPatient().getPatientID();
                row[1] = req.getPatient().getName();
                row[2] = req;
                if(req.getStatus().equals("Pending")){
                   row[3] = "Not Decided";
                } else{
                   row[3] = req.getPatient().getIfNeedHospitalized() == true ? "Hopitalized" : "Take Medicine";
                }
                row[4] = req.getStatus();
                model.addRow(row);
            }
        }
        
    }
    
    private void populateOperationCombo(){
        operationJComboBox.removeAllItems();
        operationJComboBox.addItem("Need Hospitalize");
        operationJComboBox.addItem("Take Medicine");
    }
    
    private void populateReceiverCombo(String operation){
        receiverJComboBox.removeAllItems();
        if(operation.equals("Need Hospitalize")){
            if(nurseStation.getUserAccountDirectory().getUserAccountList() != null){
                for(UserAccount ua : nurseStation.getUserAccountDirectory().getUserAccountList()){
//                if(!ua.getWorkQueue().getWorkRequestList().isEmpty()){
                    receiverJComboBox.addItem(ua);
                    
//                }
                }
            } 
            else{
                receiverJComboBox.addItem("No nurse");
            }
        }
        else if(operation.equals("Take Medicine")){
            if(pharmacy.getUserAccountDirectory().getUserAccountList() != null){
                for(UserAccount ua : pharmacy.getUserAccountDirectory().getUserAccountList()){
                    receiverJComboBox.addItem(ua);
                }
            }
            else{
                receiverJComboBox.addItem("No staff");
            }
        }
        
    }

   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        jScrollPane = new javax.swing.JScrollPane();
        patientListJTable = new javax.swing.JTable();
        confirmJButton = new javax.swing.JButton();
        backJButton = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        operationJComboBox = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        prescriptionJTextArea = new javax.swing.JTextArea();
        jLabel9 = new javax.swing.JLabel();
        receiverJComboBox = new javax.swing.JComboBox();
        jLabel10 = new javax.swing.JLabel();
        patientIDJTextField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        patientNameJTextField = new javax.swing.JTextField();

        patientListJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null},
                {null, null, null, null, null}
            },
            new String [] {
                "Patient ID", "Patient Name", "Symptom", "Result", "Status"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        patientListJTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                patientListJTableMousePressed(evt);
            }
        });
        jScrollPane.setViewportView(patientListJTable);
        if (patientListJTable.getColumnModel().getColumnCount() > 0) {
            patientListJTable.getColumnModel().getColumn(0).setPreferredWidth(30);
            patientListJTable.getColumnModel().getColumn(1).setPreferredWidth(60);
            patientListJTable.getColumnModel().getColumn(2).setPreferredWidth(120);
            patientListJTable.getColumnModel().getColumn(3).setPreferredWidth(100);
            patientListJTable.getColumnModel().getColumn(4).setPreferredWidth(60);
        }

        confirmJButton.setText("Confirm");
        confirmJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                confirmJButtonActionPerformed(evt);
            }
        });

        backJButton.setText("Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Al Bayan", 1, 16)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("View Patient List");

        jLabel6.setFont(new java.awt.Font("Al Bayan", 1, 16)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Treatment Result");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel7.setText("Operation:");

        operationJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        operationJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                operationJComboBoxActionPerformed(evt);
            }
        });

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel8.setText("Prescription:");

        prescriptionJTextArea.setColumns(20);
        prescriptionJTextArea.setRows(5);
        jScrollPane1.setViewportView(prescriptionJTextArea);

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Receiver:");

        receiverJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Patient ID:");

        patientIDJTextField.setEditable(false);

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Patient Name:");

        patientNameJTextField.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(177, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addComponent(backJButton)
                            .addGap(331, 331, 331))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(patientIDJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 221, Short.MAX_VALUE))))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(patientNameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(operationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(receiverJComboBox, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 284, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(confirmJButton, javax.swing.GroupLayout.Alignment.TRAILING)))
                        .addGap(18, 18, 18)))
                .addContainerGap(186, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(56, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jLabel6)
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(patientIDJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(patientNameJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(operationJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(receiverJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(confirmJButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(backJButton)
                .addContainerGap(75, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void confirmJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_confirmJButtonActionPerformed
        int selectedRow = patientListJTable.getSelectedRow();
        
        if (selectedRow < 0){
            return;
        }
        
        //Resolve selected request (sent by CSR)
        PatientTreatmentRequest request = (PatientTreatmentRequest)patientListJTable.getValueAt(selectedRow, 2);
        request.setStatus("Resolved");
        
        //Create new request (will be sent by doctor)
        PatientTreatmentRequest newRequest = new PatientTreatmentRequest();
        UserAccount receiverAccount = (UserAccount) receiverJComboBox.getSelectedItem();
        newRequest.setMessage(prescriptionJTextArea.getText());
        newRequest.setSender(doctorAccount);
        newRequest.setReceiver(receiverAccount);
        newRequest.setStatus("Pending");
        Patient patient = request.getPatient();       
        newRequest.setPatient(patient);
        
        //Add the work request to receiver account's work queue
        receiverAccount.getWorkQueue().getWorkRequestList().add(newRequest);
        
        //Change patient's property
        String operation = (String) operationJComboBox.getSelectedItem();
        if(operation.equals("Need Hospitalize")){
            patient.setIfNeedHospitalized(true);
            //nurseStation.getWorkQueue().getWorkRequestList().add(newRequest);
        }else if(operation.equals("Take Medicine")){
            //pharmacy.getWorkQueue().getWorkRequestList().add(newRequest);
        }
        
        //Update status of the request received by doctor
        populateTable();
    }//GEN-LAST:event_confirmJButtonActionPerformed

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void patientListJTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_patientListJTableMousePressed
        //Display the patient information in the jTextFields 
        int selectedRow = patientListJTable.getSelectedRow();
        
        if (selectedRow < 0){
            return;
        }
        
        patientIDJTextField.setText(patientListJTable.getValueAt(selectedRow,0).toString());
        patientNameJTextField.setText((String) patientListJTable.getValueAt(selectedRow,1));
        
        populateOperationCombo();
    }//GEN-LAST:event_patientListJTableMousePressed

    private void operationJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_operationJComboBoxActionPerformed
        // TODO add your handling code here:
        String operation = (String) operationJComboBox.getSelectedItem();
        if(operation != null){
            populateReceiverCombo(operation);
        }           
    }//GEN-LAST:event_operationJComboBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backJButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JButton confirmJButton;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JComboBox operationJComboBox;
    private javax.swing.JTextField patientIDJTextField;
    private javax.swing.JTable patientListJTable;
    private javax.swing.JTextField patientNameJTextField;
    private javax.swing.JTextArea prescriptionJTextArea;
    private javax.swing.JComboBox receiverJComboBox;
    // End of variables declaration//GEN-END:variables
}
