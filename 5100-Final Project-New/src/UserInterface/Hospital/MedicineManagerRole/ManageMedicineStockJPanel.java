/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.Hospital.MedicineManagerRole;

import Business.Enterprise.Enterprise;
import Business.Enterprise.EnterpriseDirectory;
import Business.Enterprise.FactoryEnterprise;
import Business.MainSystem;
import Business.Object.Bed;
import Business.Object.Patient;
import Business.Organization.HospitalPharmacyDepartment;
import Business.Organization.Organization;
import Business.Region.Region;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.Factory.FactoryInnerRequest;
import Business.WorkQueue.Hospital.PatientTreatmentRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.awt.Image;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author fuzigeng
 */
public class ManageMedicineStockJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private UserAccount medicineManagerAccount;
    private HospitalPharmacyDepartment pharmacy;
    Map<String,Integer> medicineStock;
    private Region currentRegion;
    private MainSystem system;
    private EnterpriseDirectory innerFactoryDirectory = new EnterpriseDirectory();
    private EnterpriseDirectory exterFactoryDirectory = new EnterpriseDirectory();
    
    /**
     * Creates new form ManageBedArrangementJPanel
     */
    public ManageMedicineStockJPanel(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Region region, MainSystem system) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.medicineManagerAccount = userAccount;
        this.pharmacy = (HospitalPharmacyDepartment)organization;
        this.currentRegion = region;
        this.system = system;
String pathIconAdd = "src/images/add.png";
         String pathIconBack="src/images/back.png";
     String pathIconConfirm = "src/images/confirm.png";
         ImageIcon iconAdd= new ImageIcon(pathIconAdd);
         ImageIcon iconBack = new ImageIcon(pathIconBack);
         ImageIcon iconConfirm = new ImageIcon(pathIconConfirm);
        iconConfirm.setImage(iconConfirm.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconAdd.setImage(iconAdd.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
        iconBack.setImage(iconBack.getImage().getScaledInstance(20,20,Image.SCALE_DEFAULT));
                this.backJButton.setIcon(iconBack);
        this.addStockFromFactoryJButton.setIcon(iconAdd);

        //Inner-network communication
        //Traverse the enterprise directory of the network get the research center
        for(Enterprise etp : currentRegion.getEnterpriseDirectory().getEnterpriseList()){
            if(etp.getEnterpriseType() == Enterprise.EnterpriseType.Factory){
                this.innerFactoryDirectory.getEnterpriseList().add(etp);
            }
        }
        
        //Cross-network communication
        //Traverse the enterprise directory of each network of the system and get a list of research centers
        for(Region rg : system.getRegionList()){
            //If region is not the current region
            if(!rg.equals(currentRegion)){
                for(Enterprise etp : rg.getEnterpriseDirectory().getEnterpriseList()){
                    if(etp.getEnterpriseType() == Enterprise.EnterpriseType.Factory){
                        this.exterFactoryDirectory.getEnterpriseList().add(etp);
                    }
                }
            } 
        }

        populateMedicineStockTable();
        populateTypeJCombo();

    }
    
    private void populateMedicineStockTable(){
        DefaultTableModel model = (DefaultTableModel) medicineStockJTable.getModel();   
        model.setRowCount(0);
        
        medicineStock = pharmacy.getMedicineStock();
        
        for(String medicineName : medicineStock.keySet()){
            Object[] row = new Object[2];
            row[0] = medicineName;
            row[1] = medicineStock.get(medicineName);
            
            model.addRow(row);
        }
        
    }
    
    private void populateTypeJCombo(){
        typeJComboBox.removeAllItems();
        typeJComboBox.addItem("Inner-Region");
        typeJComboBox.addItem("Cross-Region");
    }
    
    private void populateFactoryJCombo(String type){
        factoryJComboBox.removeAllItems();
        if(type.equals("Inner-Region")){
            if(!innerFactoryDirectory.getEnterpriseList().isEmpty()){
                for(Enterprise etp : innerFactoryDirectory.getEnterpriseList()){
                    factoryJComboBox.addItem(etp);
                }
            } 
            else{
                factoryJComboBox.addItem("No factory");
            }
        }
        else if(type.equals("Cross-Region")){
            if(!exterFactoryDirectory.getEnterpriseList().isEmpty()){
                for(Enterprise etp : exterFactoryDirectory.getEnterpriseList()){
                    factoryJComboBox.addItem(etp);
                }
            }
            else{
                factoryJComboBox.addItem("No factory");
            }
        }
    }
    
    private void populateFactoryMedicineJCombo(FactoryEnterprise factory){
        factoryMedicineJComboBox.removeAllItems();
        ArrayList<WorkRequest> directory = null;
        
        for(Organization org : factory.getOrganizationDirectory().getOrganizationList()){
            if(org.getOrganizationType() == Organization.OrganizationType.MarketingDepartment){
                directory = org.getWorkQueue().getWorkRequestList();
            }
        }
        if(directory != null){
            for(WorkRequest factoryInnerReq : directory){
                factoryMedicineJComboBox.addItem(factoryInnerReq);
            }
        } 

    }
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        backJButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        medicineStockJTable = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        typeJComboBox = new javax.swing.JComboBox();
        jLabel17 = new javax.swing.JLabel();
        factoryJComboBox = new javax.swing.JComboBox();
        jLabel18 = new javax.swing.JLabel();
        factoryMedicineQuantityJTextField = new javax.swing.JTextField();
        addStockFromFactoryJButton = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        factoryMedicineJComboBox = new javax.swing.JComboBox();

        backJButton.setText("Back");
        backJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJButtonActionPerformed(evt);
            }
        });

        medicineStockJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null},
                {null, null},
                {null, null},
                {null, null}
            },
            new String [] {
                "Medicine Name", "Quantity in Stock"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        medicineStockJTable.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                medicineStockJTableMousePressed(evt);
            }
        });
        jScrollPane2.setViewportView(medicineStockJTable);

        jLabel9.setFont(new java.awt.Font("Al Bayan", 1, 16)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Medicine Stock Information");

        jLabel13.setFont(new java.awt.Font("Al Bayan", 1, 16)); // NOI18N
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Import Medicine From Factory");

        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel16.setText("Type:");

        typeJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        typeJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeJComboBoxActionPerformed(evt);
            }
        });

        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Factory:");

        factoryJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        factoryJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                factoryJComboBoxActionPerformed(evt);
            }
        });

        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel18.setText("Quantity:");

        addStockFromFactoryJButton.setText("Add");
        addStockFromFactoryJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addStockFromFactoryJButtonActionPerformed(evt);
            }
        });

        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel19.setText("Medicine:");

        factoryMedicineJComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        factoryMedicineJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                factoryMedicineJComboBoxActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 122, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 573, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(backJButton, javax.swing.GroupLayout.Alignment.LEADING))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 573, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.LEADING))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(28, 28, 28)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(typeJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(factoryJComboBox, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(factoryMedicineJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(28, 28, 28)
                                .addComponent(factoryMedicineQuantityJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(154, 154, 154))
                    .addComponent(addStockFromFactoryJButton, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(127, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(66, Short.MAX_VALUE)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(typeJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(factoryJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(factoryMedicineJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(factoryMedicineQuantityJTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addComponent(addStockFromFactoryJButton)
                .addGap(25, 25, 25)
                .addComponent(backJButton)
                .addContainerGap(115, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void backJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJButtonActionPerformed

        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backJButtonActionPerformed

    private void medicineStockJTableMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_medicineStockJTableMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_medicineStockJTableMousePressed

    private void addStockFromFactoryJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addStockFromFactoryJButtonActionPerformed
        // TODO add your handling code here:
        FactoryInnerRequest factoryInnerReq = (FactoryInnerRequest)factoryMedicineJComboBox.getSelectedItem();
        
        int quantityToAdd = 0;
        if(factoryMedicineQuantityJTextField.getText().isEmpty()){
            JOptionPane.showMessageDialog(null, "Medicine quantity cannot be empty!");
            return;
        }
        
        try{
            quantityToAdd = Integer.parseInt(factoryMedicineQuantityJTextField.getText());
        }catch(NumberFormatException e){
            JOptionPane.showMessageDialog(null, "Medicine quantity must be a number!");
            return;
        }
        
        if(quantityToAdd > factoryInnerReq.getQuantity()){
            JOptionPane.showMessageDialog(null, "Factory only has " + factoryInnerReq.getQuantity() + " " + factoryInnerReq);
            return;
        }else{
            if(pharmacy.getMedicineStock().containsKey(factoryInnerReq.getDrugName())){
                int medicineStock = pharmacy.getMedicineStock().get(factoryInnerReq.getDrugName());
                medicineStock += quantityToAdd;
                pharmacy.getMedicineStock().put(factoryInnerReq.getDrugName(), medicineStock);
                JOptionPane.showMessageDialog(null, "Phurchase medicine successfully!");
            }else{
                pharmacy.getMedicineStock().put(factoryInnerReq.getDrugName(), quantityToAdd);
                JOptionPane.showMessageDialog(null, "Phurchase medicine successfully!");
            }

            int updatedCount = factoryInnerReq.getQuantity() - quantityToAdd;

            factoryInnerReq.setQuantity(updatedCount);

            populateMedicineStockTable();
        }
    
    }//GEN-LAST:event_addStockFromFactoryJButtonActionPerformed

    private void typeJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeJComboBoxActionPerformed
        // TODO add your handling code here:
        String type = (String) typeJComboBox.getSelectedItem();
        if(type != null){
            populateFactoryJCombo(type);
        }   
    }//GEN-LAST:event_typeJComboBoxActionPerformed

    private void factoryJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_factoryJComboBoxActionPerformed
        // TODO add your handling code here:
        FactoryEnterprise factory = (FactoryEnterprise)factoryJComboBox.getSelectedItem();
        if(factory != null){
            populateFactoryMedicineJCombo(factory);
        } 
    }//GEN-LAST:event_factoryJComboBoxActionPerformed

    private void factoryMedicineJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_factoryMedicineJComboBoxActionPerformed
        // TODO add your handling code here:
        FactoryInnerRequest factoryInnerReq = (FactoryInnerRequest)factoryMedicineJComboBox.getSelectedItem();
        //diseaseTreatedJTextField.setText(factoryInnerReq.getDisease());
    }//GEN-LAST:event_factoryMedicineJComboBoxActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addStockFromFactoryJButton;
    private javax.swing.JButton backJButton;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JComboBox factoryJComboBox;
    private javax.swing.JComboBox factoryMedicineJComboBox;
    private javax.swing.JTextField factoryMedicineQuantityJTextField;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable medicineStockJTable;
    private javax.swing.JComboBox typeJComboBox;
    // End of variables declaration//GEN-END:variables
}
