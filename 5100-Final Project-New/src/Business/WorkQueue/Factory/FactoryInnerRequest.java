/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.Factory;

import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkRequest;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * @author lx
 */
public class FactoryInnerRequest extends WorkRequest{
    private String drugName;
    private String disease;
    private int quantity;
    private UserAccount sender; 
    private UserAccount reciever;

    HashMap<String,Integer> materialStock;
    HashMap<String,Integer> materialQuantity;
//    HashMap<UserAccount, String> workerTask;
    public FactoryInnerRequest(String drugName, String disease, HashMap<String,Integer> materialQuantity, HashMap<String, Integer> materialStock) {
        this.drugName = drugName;
        this.disease = disease;
        this.materialStock = materialStock;
        this.materialQuantity = materialQuantity;
//        workerTask = new HashMap<>();
    }
    
    @Override
    public String toString(){
        return this.drugName;
    }

    public HashMap<String, Integer> getMaterialQuantity() {
        return materialQuantity;
    }

//    public HashMap<UserAccount, String> getWorkerTask() {
//        return workerTask;
//    }
//
//    public void setWorkerTask(HashMap<UserAccount, String> workerTask) {
//        this.workerTask = workerTask;
//    }
    public void setMaterialQuantity(HashMap<String, Integer> materialQuantity) {
        this.materialQuantity = materialQuantity;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public HashMap<String, Integer> getMaterialStock() {
        return materialStock;
    }

    public void setMaterialStock(HashMap<String, Integer> map) {
        this.materialStock = materialStock;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public UserAccount getReciever() {
        return reciever;
    }

    public void setReciever(UserAccount reciever) {
        this.reciever = reciever;
    }
    
}
