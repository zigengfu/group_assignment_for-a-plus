/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.Factory;
import java.util.ArrayList;

/**
 *
 * @author lx
 */
public class FactoryInnerReqDirectory {
    ArrayList<FactoryInnerRequest> factoryInnerReqDirectory;

    public FactoryInnerReqDirectory() {
        this.factoryInnerReqDirectory = new ArrayList();
    }

    public ArrayList<FactoryInnerRequest> getFactoryInnerReqDirectory() {
        return factoryInnerReqDirectory;
    }

    public void setFactoryInnerReqDirectory(ArrayList<FactoryInnerRequest> factoryInnerReqDirectory) {
        this.factoryInnerReqDirectory = factoryInnerReqDirectory;
    }
    
     
}
