/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.Factory;

import Business.Enterprise.Enterprise;
import Business.WorkQueue.WorkRequest;

/**
 *
 * @author fuzigeng
 */
public class NotificationRequest extends WorkRequest {
    private Enterprise receiverEnterprise;
    private Enterprise senderEnterprise;

    public Enterprise getReceiverEnterprise() {
        return receiverEnterprise;
    }

    public void setReceiverEnterprise(Enterprise receiverEnterprise) {
        this.receiverEnterprise = receiverEnterprise;
    }

    public Enterprise getSenderEnterprise() {
        return senderEnterprise;
    }

    public void setSenderEnterprise(Enterprise senderEnterprise) {
        this.senderEnterprise = senderEnterprise;
    }
    
    @Override
    public String toString(){
        return this.getSenderEnterprise().getName();
    }
    
}
