/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.ResearchCenter;

import Business.Object.DiseaseSample;
import Business.Object.Formulation;
import Business.WorkQueue.WorkRequest;
import java.util.ArrayList;

/**
 *
 * @author fuzigeng
 */
public class SupervisionRequest extends WorkRequest{
    public ArrayList<Formulation> formulation;
    public DiseaseSample disease;
    public SupervisionRequest(DiseaseSample disease){
        super();
        formulation=new ArrayList<Formulation>();
        this.disease=disease;
    }

    public DiseaseSample getDisease() {
        return disease;
    }

    public void setDisease(DiseaseSample disease) {
        this.disease = disease;
    }

    public ArrayList<Formulation> getFormulation() {
        return formulation;
    }

    public void setFormulation(ArrayList<Formulation> formulation) {
        this.formulation = formulation;
    }



    
    @Override
    public String toString(){
        return disease.toString();
    }
    
}
