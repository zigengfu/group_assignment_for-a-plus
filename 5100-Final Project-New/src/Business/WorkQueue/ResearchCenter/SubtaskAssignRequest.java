/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.ResearchCenter;

import Business.Object.DiseaseSample;
import Business.Object.Formulation;
import Business.WorkQueue.WorkRequest;

/**
 *
 * @author fuzigeng
 */
public class SubtaskAssignRequest extends WorkRequest{
    public Formulation formulation;
    public DiseaseSample disease;
    public SubtaskAssignRequest(DiseaseSample disease){
        super();
        formulation=new Formulation();
        this.disease=disease;
    }

    public DiseaseSample getDisease() {
        return disease;
    }

    public void setDisease(DiseaseSample disease) {
        this.disease = disease;
    }

    public Formulation getFormulation() {
        return formulation;
    }

    public void setFormulation(Formulation formulation) {
        this.formulation = formulation;
    }

    
    @Override
    public String toString(){
        return formulation.getDisease();
    }

    
}
