/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.Hospital;

import Business.Object.Patient;
import Business.WorkQueue.WorkRequest;

/**
 *
 * @author fuzigeng
 */
public class PatientTreatmentRequest extends WorkRequest{
    private Patient patient;

    public Patient getPatient() {
        return patient;
    }

    public void setPatient(Patient patient) {
        this.patient = patient;
    }
    
    @Override
    public String toString(){
        return this.getMessage();
    }
    
}
