/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue.Hospital;

import Business.Enterprise.Enterprise;
import Business.Object.DiseaseSample;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkRequest;
import java.util.Date;

/**
 *
 * @author fuzigeng
 */
public class DiseaseSampleRequest extends WorkRequest{
    private DiseaseSample ds;
    private Enterprise receiverEnterprise;
    private Enterprise senderEnterprise;

    public DiseaseSample getDs() {
        return ds;
    }

    public void setDs(DiseaseSample ds) {
        this.ds = ds;
    }

    public Enterprise getReceiverEnterprise() {
        return receiverEnterprise;
    }

    public void setReceiverEnterprise(Enterprise receiverEnterprise) {
        this.receiverEnterprise = receiverEnterprise;
    }

    public Enterprise getSenderEnterprise() {
        return senderEnterprise;
    }

    public void setSenderEnterprise(Enterprise senderEnterprise) {
        this.senderEnterprise = senderEnterprise;
    }
    
    @Override
    public String toString(){
        return this.ds.getDiseaseName();
    }

    
}
