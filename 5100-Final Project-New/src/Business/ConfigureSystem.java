/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import Business.Employee.Employee;
import Business.Enterprise.Enterprise;
import Business.Object.Patient;
import Business.Organization.Organization;
import Business.Region.Region;
import Business.Role.Factory.FactoryAdminRole;
import Business.Role.Factory.FactoryMarketingAdminRole;
import Business.Role.Factory.FactoryProduceAdminRole;
import Business.Role.Factory.FactorySupplyAdminRole;
import Business.Role.Factory.FactoryWorkerRole;
import Business.Role.Hospital.HospitalAdmin;
import Business.Role.Hospital.HospitalCSR;
import Business.Role.Hospital.HospitalDoctor;
import Business.Role.Hospital.HospitalMedicineManager;
import Business.Role.Hospital.HospitalNurse;
import Business.Role.ResearchCenter.ResearchAdmin;
import Business.Role.ResearchCenter.ResearchLabAssistant;
import Business.Role.ResearchCenter.ResearchProfessor;
import Business.Role.ResearchCenter.ResearchSampleManager;
import Business.Role.ResearchCenter.ResearchSupervisor;
import Business.Role.Role;
/**
 *
 * @author lx
 */
public class ConfigureSystem {
    public static MainSystem configure(){
        MainSystem system = MainSystem.getInstance();

        //Create system admin user account
        UserAccount user = system.getUserAccountDirectory().createUserAccount("admin", "admin", new Employee(), new SystemAdminRole());

//        //Create first networks
//        Region ca = system.createRegion();
//        ca.setName("CA");
//
//        //Create three enterprises in CA network
//        Enterprise caHospital1 = ca.getEnterpriseDirectory().createAndAddEnterprise("CA State Hospital", Enterprise.EnterpriseType.Hospital);
//        Enterprise caResearchCenter1 = ca.getEnterpriseDirectory().createAndAddEnterprise("CA State Research Ctr.", Enterprise.EnterpriseType.ResearchCenter);
//        Enterprise caFactory1 = ca.getEnterpriseDirectory().createAndAddEnterprise("CA State Medicine Factory", Enterprise.EnterpriseType.Factory);
//
//        //Create 3 enterprise admin user accounts
//        Employee cah1e0 = caHospital1.getEmployeeDirectory().createEmployee("Zhang San");
//        UserAccount cah1admin = caHospital1.getUserAccountDirectory().createUserAccount("cah1admin", "cah1admin" , cah1e0, new HospitalAdmin());
//
//        Employee cars1e0 = caResearchCenter1.getEmployeeDirectory().createEmployee("Li Si");
//        UserAccount cars1admin = caResearchCenter1.getUserAccountDirectory().createUserAccount("cars1admin", "cars1admin" , cars1e0, new ResearchAdmin());
//
//        Employee caf1e0 = caFactory1.getEmployeeDirectory().createEmployee("Wang Wu");
//        UserAccount caf1admin = caFactory1.getUserAccountDirectory().createUserAccount("caf1admin", "caf1admin", caf1e0, new FactoryAdminRole());
//
//        //Create 4 organizations for CA state hospital
//        Organization caHospital1Regis = caHospital1.getOrganizationDirectory().createOrganization("Registration Center", Organization.OrganizationType.RegistrationDepartment);
//        Organization caHospital1Outpatient = caHospital1.getOrganizationDirectory().createOrganization("Outpatient Department", Organization.OrganizationType.OutpatientDepartment);
//        Organization caHospital1Pharmacy= caHospital1.getOrganizationDirectory().createOrganization("Pharmacy", Organization.OrganizationType.Pharmacy);
//        Organization caHospital1NurseStation= caHospital1.getOrganizationDirectory().createOrganization("Nurse Station", Organization.OrganizationType.NurseStation);
//
//        //Create 4 organizations (2 different labs) for CA state research center
//        Organization caResearch1Info = caResearchCenter1.getOrganizationDirectory().createOrganization("Information Department", Organization.OrganizationType.InformationDepartment);
//        Organization caResearch1Lab1 = caResearchCenter1.getOrganizationDirectory().createOrganization("Stevens Heart Lab", Organization.OrganizationType.LabDepartment);
//        Organization caResearch1Lab2 = caResearchCenter1.getOrganizationDirectory().createOrganization("Micheal Skeleton Lab", Organization.OrganizationType.LabDepartment);
//        Organization caResearch1Supervision = caResearchCenter1.getOrganizationDirectory().createOrganization("Supervision Center", Organization.OrganizationType.SupervisionDepartment);
//
//        //Create 3 organizations for CA state medicine factory
//        Organization caFactory1Supply = caFactory1.getOrganizationDirectory().createOrganization("Supplyment Department", Organization.OrganizationType.SupplyDepartment);
//        Organization caFactory1Produce = caFactory1.getOrganizationDirectory().createOrganization("Production Department", Organization.OrganizationType.ProductionDepartment);
//        Organization caFactory1Marketing = caFactory1.getOrganizationDirectory().createOrganization("Marketing Department", Organization.OrganizationType.MarketingDepartment);
//
//        //Create employees for CA state hospital
//        //Create employee and user accounts for CA Hospital 1 Registration Center
//        Employee cah1e1 = caHospital1Regis.getEmployeeDirectory().createEmployee("Louis Brown");
//        UserAccount cah1rg1 = caHospital1Regis.getUserAccountDirectory().createUserAccount("cah1rg1", "cah1rg1", cah1e1, new HospitalCSR());
//
//        //Create employee and user accounts for CA Hospital 1 Outpatient Department
//        Employee cah1e2 = caHospital1Outpatient.getEmployeeDirectory().createEmployee("John Brown");
//        UserAccount cah1dr1 = caHospital1Outpatient.getUserAccountDirectory().createUserAccount("cah1dr1", "cah1dr1", cah1e2, new HospitalDoctor());
//
//        Employee cah1e3 = caHospital1Outpatient.getEmployeeDirectory().createEmployee("Smith Brown");
//        UserAccount cah1dr2 = caHospital1Outpatient.getUserAccountDirectory().createUserAccount("cah1dr2", "cah1dr2", cah1e3, new HospitalDoctor());
//
//        Employee cah1e4 = caHospital1Outpatient.getEmployeeDirectory().createEmployee("Lucy White");
//        UserAccount cah1dr3 = caHospital1Outpatient.getUserAccountDirectory().createUserAccount("cah1dr3", "cah1dr3", cah1e4, new HospitalDoctor());
//
//        //Create employee and user accounts for CA Hospital 1 Nurse Station
//        Employee cah1e5 = caHospital1NurseStation.getEmployeeDirectory().createEmployee("Lucy Smile");
//        UserAccount cah1nrs1 = caHospital1NurseStation.getUserAccountDirectory().createUserAccount("cah1nrs1", "cah1nrs1", cah1e5, new HospitalNurse());
//
//        Employee cah1e6 = caHospital1NurseStation.getEmployeeDirectory().createEmployee("Mary Smile");
//        UserAccount cah1nrs2 = caHospital1NurseStation.getUserAccountDirectory().createUserAccount("cah1nrs2", "cah1nrs2", cah1e6, new HospitalNurse());
//
//        Employee cah1e7 = caHospital1NurseStation.getEmployeeDirectory().createEmployee("Simon Smile");
//        UserAccount cah1nrs3 = caHospital1NurseStation.getUserAccountDirectory().createUserAccount("cah1nrs3", "cah1nrs3", cah1e7, new HospitalNurse());
//
//        //Create employee and user accounts for CA Hospital 1 Pharmacy
//        Employee cah1e8 = caHospital1Pharmacy.getEmployeeDirectory().createEmployee("Louis Black");
//        UserAccount cah1mm1 = caHospital1Pharmacy.getUserAccountDirectory().createUserAccount("cah1mm1", "cah1mm1", cah1e8, new HospitalMedicineManager());
//
//        //Create employees for CA state research center
//        //Create employee and user accounts for CA Research Center 1 Information Center
//        Employee cars1e1 = caResearch1Info.getEmployeeDirectory().createEmployee("John Doe");
//        UserAccount cars1sm1 =caResearch1Info.getUserAccountDirectory().createUserAccount("cars1sm1", "cars1sm1", cars1e1, new ResearchSampleManager());
//
//        //Create employee and user accounts for CA Research Center 1 Lab1
//        Employee cars1e2 = caResearch1Lab1.getEmployeeDirectory().createEmployee("Smith Doe");
//        UserAccount cars1prof1 = caResearch1Lab1.getUserAccountDirectory().createUserAccount("cars1prof1", "cars1prof1", cars1e2, new ResearchProfessor());
//
//        Employee cars1e4 = caResearch1Lab1.getEmployeeDirectory().createEmployee("Simon Doe");
//        UserAccount cars1ass1 = caResearch1Lab1.getUserAccountDirectory().createUserAccount("cars1ass1", "cars1ass1", cars1e4, new ResearchLabAssistant());
//
//        Employee cars1e5 = caResearch1Lab1.getEmployeeDirectory().createEmployee("Liu He");
//        UserAccount cars1ass2 = caResearch1Lab1.getUserAccountDirectory().createUserAccount("cars1ass2", "cars1ass2", cars1e5, new ResearchLabAssistant());
//
//        //Create employee and user accounts for CA Research Center 1 Lab2
//        Employee cars1e3 = caResearch1Lab2.getEmployeeDirectory().createEmployee("Zhang Chi");
//        UserAccount cars1prof2 = caResearch1Lab2.getUserAccountDirectory().createUserAccount("cars1prof2", "cars1prof2", cars1e3, new ResearchProfessor());
//
//        Employee cars1e6 = caResearch1Lab2.getEmployeeDirectory().createEmployee("Liu He");
//        UserAccount cars1ass3 = caResearch1Lab2.getUserAccountDirectory().createUserAccount("cars1ass3", "cars1ass3", cars1e6, new ResearchLabAssistant());
//
//        Employee cars1e7 = caResearch1Lab2.getEmployeeDirectory().createEmployee("Zhao Mei");
//        UserAccount cars1ass4 = caResearch1Lab2.getUserAccountDirectory().createUserAccount("cars1ass4", "cars1ass4", cars1e7, new ResearchLabAssistant());
//
//        //Create employee and user accounts for CA Research Center 1 Supercision Department
//        Employee cars1e8 = caResearch1Supervision.getEmployeeDirectory().createEmployee("Li Xin");
//        UserAccount cars1sp1 = caResearch1Supervision.getUserAccountDirectory().createUserAccount("cars1sp1", "cars1sp1", cars1e8, new ResearchSupervisor());
//
//        //Create employees for CA state factory
//        //Create employee and user accounts for CA Factory 1 Supplyment Department
//        Employee caf1e1 = caFactory1Supply.getEmployeeDirectory().createEmployee("Zheng Kai");
//        UserAccount caf1sp1 = caFactory1Supply.getUserAccountDirectory().createUserAccount("caf1sp1", "caf1sp1", caf1e1, new FactorySupplyAdminRole());
//
//        //Create employee and user accounts for CA Factory 1 Production Department (1 produce admin and 2 workers)
//        Employee caf1e2 = caFactory1Produce.getEmployeeDirectory().createEmployee("Li Chen");
//        UserAccount caf1pd1 = caFactory1Produce.getUserAccountDirectory().createUserAccount("caf1pd1", "caf1pd1", caf1e2, new FactoryProduceAdminRole());
//
//        Employee caf1e3 = caFactory1Produce.getEmployeeDirectory().createEmployee("Liu Chang");
//        UserAccount caf1wk1 = caFactory1Produce.getUserAccountDirectory().createUserAccount("caf1wk1", "caf1wk1", caf1e3, new FactoryWorkerRole());
//
//        Employee caf1e4 = caFactory1Produce.getEmployeeDirectory().createEmployee("Yu Ping");
//        UserAccount caf1wk2 = caFactory1Produce.getUserAccountDirectory().createUserAccount("caf1wk2", "caf1wk2", caf1e4, new FactoryWorkerRole());
//
//        //Create employee and user accounts for CA Factory 1 Marketing Department
//        Employee caf1e5 = caFactory1Marketing.getEmployeeDirectory().createEmployee("Peter Pan");
//        UserAccount caf1mk1 = caFactory1Marketing.getUserAccountDirectory().createUserAccount("caf1mk1", "caf1mk1", caf1e5, new FactoryMarketingAdminRole());
//
//
//
//        //Yityong write the creation of another region (MA) below...Keep the naming format!
//        Region ma = system.createRegion();
//        ma.setName("MA");
//
//        //Create three enterprises in MA network
//        Enterprise maHospital1 = ma.getEnterpriseDirectory().createAndAddEnterprise("MA State Hospital", Enterprise.EnterpriseType.Hospital);
//        Enterprise maResearchCenter1 = ma.getEnterpriseDirectory().createAndAddEnterprise("MA State Research Ctr.", Enterprise.EnterpriseType.ResearchCenter);
//        Enterprise maFactory1 = ma.getEnterpriseDirectory().createAndAddEnterprise("MA State Medicine Factory", Enterprise.EnterpriseType.Factory);
//
//        //Create 3 enterprise admin user accounts
//        Employee mah1e0 = maHospital1.getEmployeeDirectory().createEmployee("Yitong Liu");
//        UserAccount mah1admin = maHospital1.getUserAccountDirectory().createUserAccount("mah1admin", "mah1admin" , mah1e0, new HospitalAdmin());
//
//        Employee mars1e0 = maResearchCenter1.getEmployeeDirectory().createEmployee("Zheng Wang");
//        UserAccount mars1admin = maResearchCenter1.getUserAccountDirectory().createUserAccount("mars1admin", "mars1admin" , mars1e0, new ResearchAdmin());
//
//        Employee maf1e0 = maFactory1.getEmployeeDirectory().createEmployee("Song Qi");
//        UserAccount maf1admin = maFactory1.getUserAccountDirectory().createUserAccount("maf1admin", "maf1admin", maf1e0, new FactoryAdminRole());
//
//        //Create 4 organizations for MA state hospital
//        Organization maHospital1Regis = maHospital1.getOrganizationDirectory().createOrganization("Registration Center", Organization.OrganizationType.RegistrationDepartment);
//        Organization maHospital1Outpatient = maHospital1.getOrganizationDirectory().createOrganization("Outpatient Department", Organization.OrganizationType.OutpatientDepartment);
//        Organization maHospital1Pharmacy= maHospital1.getOrganizationDirectory().createOrganization("Pharmacy", Organization.OrganizationType.Pharmacy);
//        Organization maHospital1NurseStation= maHospital1.getOrganizationDirectory().createOrganization("Nurse Station", Organization.OrganizationType.NurseStation);
//
//        //Create 4 organizations (2 different labs) for MA state research center
//        Organization maResearch1Info = maResearchCenter1.getOrganizationDirectory().createOrganization("Information Department", Organization.OrganizationType.InformationDepartment);
//        Organization maResearch1Lab1 = maResearchCenter1.getOrganizationDirectory().createOrganization("Jeff Brain Lab", Organization.OrganizationType.LabDepartment);
//        Organization maResearch1Lab2 = maResearchCenter1.getOrganizationDirectory().createOrganization("Docker Orthopedic Lab", Organization.OrganizationType.LabDepartment);
//        Organization maResearch1Supervision = maResearchCenter1.getOrganizationDirectory().createOrganization("Supervision Center", Organization.OrganizationType.SupervisionDepartment);
//
//        //Create 3 organizations for MA state medicine factory
//        Organization maFactory1Supply = maFactory1.getOrganizationDirectory().createOrganization("Supplyment Department", Organization.OrganizationType.SupplyDepartment);
//        Organization maFactory1Produce = maFactory1.getOrganizationDirectory().createOrganization("Production Department", Organization.OrganizationType.ProductionDepartment);
//        Organization maFactory1Marketing = maFactory1.getOrganizationDirectory().createOrganization("Marketing Department", Organization.OrganizationType.MarketingDepartment);
//
//        //Create employees for MA state hospital
//        //Create employee and user accounts for MA Hospital 1 Registration Center
//        Employee mah1e1 = maHospital1Regis.getEmployeeDirectory().createEmployee("Jacky Chen");
//        UserAccount mah1rg1 = maHospital1Regis.getUserAccountDirectory().createUserAccount("mah1rg1", "mah1rg1", mah1e1, new HospitalCSR());
//
//        //Create employee and user accounts for MA Hospital 1 Outpatient Department
//        Employee mah1e2 = maHospital1Outpatient.getEmployeeDirectory().createEmployee("Lily Brown");
//        UserAccount mah1dr1 = maHospital1Outpatient.getUserAccountDirectory().createUserAccount("mah1dr1", "mah1dr1", mah1e2, new HospitalDoctor());
//
//        Employee mah1e3 = maHospital1Outpatient.getEmployeeDirectory().createEmployee("John Richard");
//        UserAccount mah1dr2 = maHospital1Outpatient.getUserAccountDirectory().createUserAccount("mah1dr2", "mah1dr2", mah1e3, new HospitalDoctor());
//
//        Employee mah1e4 = maHospital1Outpatient.getEmployeeDirectory().createEmployee("Vere Wang");
//        UserAccount mah1dr3 = maHospital1Outpatient.getUserAccountDirectory().createUserAccount("mah1dr3", "mah1dr3", mah1e4, new HospitalDoctor());
//
//        //Create employee and user accounts for MA Hospital 1 Nurse Station
//        Employee mah1e5 = maHospital1NurseStation.getEmployeeDirectory().createEmployee("Alexander Mcqueen");
//        UserAccount mah1nrs1 = maHospital1NurseStation.getUserAccountDirectory().createUserAccount("mah1nrs1", "mah1nrs1", mah1e5, new HospitalNurse());
//
//        Employee mah1e6 = maHospital1NurseStation.getEmployeeDirectory().createEmployee("Mary Smile");
//        UserAccount mah1nrs2 = maHospital1NurseStation.getUserAccountDirectory().createUserAccount("mah1nrs2", "mah1nrs2", mah1e6, new HospitalNurse());
//
//        Employee mah1e7 = maHospital1NurseStation.getEmployeeDirectory().createEmployee("Simon Lee");
//        UserAccount mah1nrs3 = maHospital1NurseStation.getUserAccountDirectory().createUserAccount("mah1nrs3", "mah1nrs3", mah1e7, new HospitalNurse());
//
//        //Create employee and user accounts for CA Hospital 1 Pharmacy
//        Employee mah1e8 = maHospital1Pharmacy.getEmployeeDirectory().createEmployee("Louis Vitton");
//        UserAccount mah1mm1 = maHospital1Pharmacy.getUserAccountDirectory().createUserAccount("mah1mm1", "mah1mm1", mah1e8, new HospitalMedicineManager());
//
//        //Create employees for MA state research center
//        //Create employee and user accounts for MA Research Center 1 Information Center
//        Employee mars1e1 = maResearch1Info.getEmployeeDirectory().createEmployee("Jimmy Choo");
//        UserAccount mars1sm1 =maResearch1Info.getUserAccountDirectory().createUserAccount("mars1sm1", "mars1sm1", mars1e1, new ResearchSampleManager());
//
//        //Create employee and user accounts for MA Research Center 1 Lab1
//        Employee mars1e2 = maResearch1Lab1.getEmployeeDirectory().createEmployee("Alexendar Wang");
//        UserAccount mars1prof1 = maResearch1Lab1.getUserAccountDirectory().createUserAccount("mars1prof1", "mars1prof1", mars1e2, new ResearchProfessor());
//
//        Employee mars1e4 = maResearch1Lab1.getEmployeeDirectory().createEmployee("David Kee");
//        UserAccount mars1ass1 = maResearch1Lab1.getUserAccountDirectory().createUserAccount("mars1ass1", "mars1ass1", mars1e4, new ResearchLabAssistant());
//
//        Employee mars1e5 = maResearch1Lab1.getEmployeeDirectory().createEmployee("Yu Hui");
//        UserAccount mars1ass2 = maResearch1Lab1.getUserAccountDirectory().createUserAccount("mars1ass2", "mars1ass2", mars1e5, new ResearchLabAssistant());
//
//        //Create employee and user accounts for MA Research Center 1 Lab2
//        Employee mars1e3 = maResearch1Lab2.getEmployeeDirectory().createEmployee("Zhang Yi");
//        UserAccount mars1prof2 = maResearch1Lab2.getUserAccountDirectory().createUserAccount("mars1prof2", "mars1prof2", mars1e3, new ResearchProfessor());
//
//        Employee mars1e6 = maResearch1Lab2.getEmployeeDirectory().createEmployee("Yu Lun");
//        UserAccount mars1ass3 = maResearch1Lab2.getUserAccountDirectory().createUserAccount("mars1ass3", "mars1ass3", mars1e6, new ResearchLabAssistant());
//
//        Employee mars1e7 = maResearch1Lab2.getEmployeeDirectory().createEmployee("Yue Ming");
//        UserAccount mars1ass4 = maResearch1Lab2.getUserAccountDirectory().createUserAccount("mars1ass4", "mars1ass4", mars1e7, new ResearchLabAssistant());
//
//        //Create employee and user accounts for MA Research Center 1 Supercision Department
//        Employee mars1e8 = maResearch1Supervision.getEmployeeDirectory().createEmployee("Hong Li");
//        UserAccount mars1sp1 = maResearch1Supervision.getUserAccountDirectory().createUserAccount("mars1sp1", "mars1sp1", mars1e8, new ResearchSupervisor());
//
//        //Create employees for MA state factory
//        //Create employee and user accounts for MA Factory 1 Supplyment Department
//        Employee maf1e1 = maFactory1Supply.getEmployeeDirectory().createEmployee("Xiang Yi");
//        UserAccount maf1sp1 = maFactory1Supply.getUserAccountDirectory().createUserAccount("maf1sp1", "maf1sp1", maf1e1, new FactorySupplyAdminRole());
//
//        //Create employee and user accounts for CA Factory 1 Production Department (1 produce admin and 2 workers)
//        Employee maf1e2 = maFactory1Produce.getEmployeeDirectory().createEmployee("Yang Ying");
//        UserAccount maf1pd1 = maFactory1Produce.getUserAccountDirectory().createUserAccount("maf1pd1", "maf1pd1", maf1e2, new FactoryProduceAdminRole());
//
//        Employee maf1e3 = maFactory1Produce.getEmployeeDirectory().createEmployee("Li Xiang");
//        UserAccount maf1wk1 = maFactory1Produce.getUserAccountDirectory().createUserAccount("maf1wk1", "maf1wk1", maf1e3, new FactoryWorkerRole());
//
//        Employee maf1e4 = maFactory1Produce.getEmployeeDirectory().createEmployee("Raj Le");
//        UserAccount maf1wk2 = maFactory1Produce.getUserAccountDirectory().createUserAccount("maf1wk2", "maf1wk2", maf1e4, new FactoryWorkerRole());
//
//        //Create employee and user accounts for CA Factory 1 Marketing Department
//        Employee maf1e5 = maFactory1Marketing.getEmployeeDirectory().createEmployee("Sheldon Pan");
//        UserAccount maf1mk1 = maFactory1Marketing.getUserAccountDirectory().createUserAccount("maf1mk1", "maf1mk1", maf1e5, new FactoryMarketingAdminRole());

        return system;
    }
}
