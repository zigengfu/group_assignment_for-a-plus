/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Enterprise.Enterprise.EnterpriseType;
import Business.Object.PatientDirectory;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author lx
 */
public class HospitalEnterprise extends Enterprise {
    private PatientDirectory patientDirectory;
    
    public HospitalEnterprise(String name){
        super(name,EnterpriseType.Hospital);
        this.patientDirectory = new PatientDirectory();
    }

    public PatientDirectory getPatientDirectory() {
        return patientDirectory;
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }
}
