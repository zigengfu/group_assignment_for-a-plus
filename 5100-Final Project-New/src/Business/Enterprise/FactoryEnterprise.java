/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Role.Role;
import Business.WorkQueue.Factory.FactoryInnerReqDirectory;
import java.util.ArrayList;

/**
 *
 * @author lx
 */
public class FactoryEnterprise extends Enterprise {
    private FactoryInnerReqDirectory factoryInnerReqDirectory;
    
    public FactoryEnterprise(String name){
        super(name,EnterpriseType.Factory);
        this.factoryInnerReqDirectory = new FactoryInnerReqDirectory();
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        return null;
    }

    public FactoryInnerReqDirectory getFactoryInnerReqDirectory() {
        return factoryInnerReqDirectory;
    }

    public void setFactoryInnerReqDirectory(FactoryInnerReqDirectory factoryInnerReqDirectory) {
        this.factoryInnerReqDirectory = factoryInnerReqDirectory;
    }

}

