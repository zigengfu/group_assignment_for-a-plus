/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import java.util.ArrayList;

/**
 *
 * @author lx
 */
public class OrganizationDirectory {
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }
    
    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(String name,Organization.OrganizationType type){
        Organization organization  = null;      
        if (type.getTypeString().equals(Organization.OrganizationType.SupplyDepartment.getTypeString())){
            organization = new FactorySupplyDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.ProductionDepartment.getTypeString())){
            organization = new FactoryProductionDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.MarketingDepartment.getTypeString())){
            organization = new FactoryMarketingDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.InformationDepartment.getTypeString())){
            organization = new ResearchInformationDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.LabDepartment.getTypeString())){
            organization = new ResearchLabDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.SupervisionDepartment.getTypeString())){
            organization = new ResearchSupervisionDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.RegistrationDepartment.getTypeString())){
            organization = new HospitalRegistrationDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.OutpatientDepartment.getTypeString())){
            organization = new HospitalOutpatientDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.NurseStation.getTypeString())){
            organization = new HospitalNurseStation(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        else if (type.getTypeString().equals(Organization.OrganizationType.Pharmacy.getTypeString())){
            organization = new HospitalPharmacyDepartment(name);
            organization.setOrganizationType(type);
            organizationList.add(organization);
        }
        
        return organization;
    }
}
