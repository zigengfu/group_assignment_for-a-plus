/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Hospital.HospitalDoctor;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author fuzigeng
 */
public class HospitalOutpatientDepartment extends Organization {
    public HospitalOutpatientDepartment (String name){
        super(name);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new HospitalDoctor());
        return roles;
    }
}
