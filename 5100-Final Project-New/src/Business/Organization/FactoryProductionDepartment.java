/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Factory.FactoryProduceAdminRole;
import Business.Role.Factory.FactoryWorkerRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author lx
 */
public class FactoryProductionDepartment extends Organization{
    public FactoryProductionDepartment(String name){
        super(name);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList<>();
        roles.add(new FactoryProduceAdminRole());
        roles.add(new FactoryWorkerRole());
        return roles;
    }
}
