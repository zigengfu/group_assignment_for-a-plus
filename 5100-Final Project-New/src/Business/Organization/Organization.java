/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Employee.EmployeeDirectory;
import Business.Enterprise.Enterprise;
import Business.Role.Role;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.Factory.FactoryInnerRequest;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author lx
 */
public abstract class Organization{

    private String name;
    private WorkQueue workQueue;
    private FactoryInnerRequest factoryInnerRequest;
    private int organizationID;
    private static int counter = 0;
    private OrganizationType organizationType;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;

   
    public Organization(String name){
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();       
        userAccountDirectory = new UserAccountDirectory();
        organizationID = counter;
        ++counter;
    }

    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
    }
    
    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public int getOrganizationID() {
        return organizationID;
    }
  
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }
    
    public abstract ArrayList<Role> getSupportedRole();

    public FactoryInnerRequest getFactoryInnerRequest() {
        return factoryInnerRequest;
    }

    public void setFactoryInnerRequest(FactoryInnerRequest factoryInnerRequest) {
        this.factoryInnerRequest = factoryInnerRequest;
    }

    public enum OrganizationType{
        SupplyDepartment("SupplyDepartment"),
        ProductionDepartment("ProductionDepartment"),
        MarketingDepartment("MarketingDepartment"),
        InformationDepartment("InformationDepartment"),
        LabDepartment("LabDepartment"),
        SupervisionDepartment("SupervisionDepartment"),
        RegistrationDepartment("RegistrationDepartment"),
        OutpatientDepartment("OutpatientDepartment"),
        NurseStation("NurseStation"),
        Pharmacy("Pharmacy");
        
        private String typeString;
        private OrganizationType(String typeString) {
            this.typeString = typeString;
        }
        public String getTypeString() {
            return typeString;
        }        
        @Override
        public String toString(){
            return typeString;
        }
    }
    
    @Override
    public String toString() {
        return name;
    } 
}
