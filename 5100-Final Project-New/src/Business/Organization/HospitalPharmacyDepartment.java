/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Hospital.HospitalMedicineManager;
import Business.Role.Role;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fuzigeng
 */
public class HospitalPharmacyDepartment extends Organization{
    private Map<String,Integer> medicineStock;
    
    public HospitalPharmacyDepartment (String name){
        super(name);
        this.medicineStock = new HashMap<>();
    }

    public Map<String, Integer> getMedicineStock() {
        return medicineStock;
    }

    public void setMedicineStock(Map<String, Integer> medicineStock) {
        this.medicineStock = medicineStock;
    }
    
    public boolean addNewMedicine(String newMedicine){
        if(this.medicineStock.containsKey(newMedicine)){
            return false; //The medicine to be added is existed in the stock
        }  
        else{
            this.medicineStock.put(newMedicine, 0);
            return true; //Add new medicine into stock successfully
        }
    }
    
    public void increaseMedicineStock(String medicine, int quantity){
        int medicineStock = this.medicineStock.get(medicine);
        medicineStock += quantity;
        this.medicineStock.put(medicine, medicineStock);
    }
    
     public void decreaseMedicineStock(String medicine, int quantity){
        int medicineStock = this.medicineStock.get(medicine);
        medicineStock -= quantity;
        this.medicineStock.put(medicine, medicineStock);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new HospitalMedicineManager());
        return roles;
    }
    
}
