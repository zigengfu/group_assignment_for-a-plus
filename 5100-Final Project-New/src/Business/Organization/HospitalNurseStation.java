/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Object.BedDirectory;
import Business.Role.Hospital.HospitalNurse;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author fuzigeng
 */
public class HospitalNurseStation extends Organization{
    private BedDirectory bedDirectory;
    
    public HospitalNurseStation (String name){
        super(name);
        this.bedDirectory = new BedDirectory();
    }

    public BedDirectory getBedDirectory() {
        return bedDirectory;
    }

    public void setBedDirectory(BedDirectory bedDirectory) {
        this.bedDirectory = bedDirectory;
    }

   @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new HospitalNurse());
        return roles;
    }
}
