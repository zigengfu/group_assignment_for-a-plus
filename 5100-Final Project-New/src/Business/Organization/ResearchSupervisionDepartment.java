/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.ResearchCenter.ResearchLabAssistant;
import Business.Role.ResearchCenter.ResearchProfessor;
import Business.Role.Role;
import Business.Role.ResearchCenter.ResearchSupervisor;
import java.util.ArrayList;

/**
 *
 * @author fuzigeng
 */
public class ResearchSupervisionDepartment extends Organization{

    public ResearchSupervisionDepartment(String name) {
        super(name);
    }
    
     @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new ResearchSupervisor());
        return roles;
    }
    
}
