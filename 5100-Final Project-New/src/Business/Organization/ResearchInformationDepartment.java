/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.Role;
import Business.Role.ResearchCenter.ResearchSampleManager;

import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author fuzigeng
 */
public class ResearchInformationDepartment extends Organization{
    
    public ResearchInformationDepartment(String name) {
        super(name);
    }

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new ResearchSampleManager());
        return roles;
    }

}
