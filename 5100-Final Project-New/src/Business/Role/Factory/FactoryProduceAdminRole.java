/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role.Factory;

import Business.Enterprise.Enterprise;
import Business.MainSystem;
import Business.Organization.Organization;
import static Business.Organization.Organization.OrganizationType.ProductionDepartment;
import Business.Organization.FactoryProductionDepartment;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import UserInterface.FactoryRoles.ProductionDepartmentJPanel;
import javax.swing.JPanel;

/**
 *
 * @author lx
 */
public class FactoryProduceAdminRole extends Role{

    @Override
    public JPanel createWorkerArea(JPanel jPanel, UserAccount userAccount, Organization organization, Enterprise enterprise, MainSystem system) {
        return new ProductionDepartmentJPanel(jPanel,userAccount,(FactoryProductionDepartment)organization,enterprise);
    }
    
}
