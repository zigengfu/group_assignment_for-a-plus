/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role.Factory;

import Business.Enterprise.Enterprise;
import Business.MainSystem;
import Business.Organization.Organization;
import static Business.Organization.Organization.OrganizationType.SupplyDepartment;
import Business.Organization.FactorySupplyDepartment;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import UserInterface.FactoryRoles.SupplyDepartmentJPanel;
import javax.swing.JPanel;

/**
 *
 * @author lx
 */
public class FactorySupplyAdminRole extends Role{

    @Override
    public JPanel createWorkerArea(JPanel jPanel, UserAccount userAccount, Organization organization, Enterprise enterprise, MainSystem system) {
        return new SupplyDepartmentJPanel(jPanel,userAccount,(FactorySupplyDepartment)organization,enterprise);
    }
    
}
