/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role.Hospital;

import Business.Enterprise.Enterprise;
import Business.MainSystem;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import UserInterface.Hospital.NurseRole.HospitalNurseWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author fuzigeng
 */
public class HospitalNurse extends Role{

    @Override
    public JPanel createWorkerArea(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, MainSystem system) {
        return new HospitalNurseWorkAreaJPanel(userProcessContainer, userAccount, organization, enterprise);
    }
  
}
