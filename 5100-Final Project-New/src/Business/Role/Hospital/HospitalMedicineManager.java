/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role.Hospital;

import Business.Enterprise.Enterprise;
import Business.MainSystem;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import UserInterface.Hospital.MedicineManagerRole.HospitalMedicineManagerWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author fuzigeng
 */
public class HospitalMedicineManager extends Role{

    @Override
    public JPanel createWorkerArea(JPanel userProcessContainer, UserAccount userAccount, Organization organization, Enterprise enterprise, MainSystem system) {
        return new HospitalMedicineManagerWorkAreaJPanel(userProcessContainer, userAccount, organization, enterprise, system);
    }
    
}
