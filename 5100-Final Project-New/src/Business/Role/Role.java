/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.Enterprise.Enterprise;
import Business.MainSystem;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import javax.swing.JPanel;

/**
 *
 * @author lx
 */
public abstract class Role {

    public enum RoleType{        
        Purchaser("Purchaser"),
        Worker("Worker"),
        Salesman("Salesman"),
        ResearchAdmin("ResearchAdmin"),
        SampleManager("SampleManager"),
        LabProfessor("LabProfessor"),
        LabAssistant("LabAssistant"),
        Supervisor("Supervisor"),
        HospitalAdmin("HospitalAdmin"),
        Doctor("Doctor"),
        Nurse("Nurse"),
        MedicineManager("MadicineManager"),
        CSR("CSR");
        
        private String value;        
        private RoleType(String value){
            this.value = value;
        }
        public String getValue(){
            return value;
        }  
        
        @Override
        public String toString() {
            return value;
        } 
           
    }
    
    public abstract JPanel createWorkerArea(JPanel jPanel,UserAccount userAccount,Organization organization,Enterprise enterprise,MainSystem system);
        
    @Override
        public String toString(){
            return this.getClass().getName();
    }
}
