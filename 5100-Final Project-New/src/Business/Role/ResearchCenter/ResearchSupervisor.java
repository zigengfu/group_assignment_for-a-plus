/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role.ResearchCenter;

import Business.Enterprise.Enterprise;
import Business.MainSystem;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.UserAccount.UserAccount;
import UserInterface.Research.LabAssistantRole.LabAssistantWorkAreaJPanel;
import UserInterface.Research.SupervisorRole.SupervisorWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author yitongliu
 */
public class ResearchSupervisor extends Role{

    @Override
    public JPanel createWorkerArea(JPanel jPanel, UserAccount userAccount, Organization organization, Enterprise enterprise, MainSystem system) {
        return new SupervisorWorkAreaJPanel(jPanel, userAccount, organization,enterprise, system);
    
}
}