/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Object;

/**
 *
 * @author fuzigeng
 */
public class Patient {
    private int patientID;
    private String name; //Writen by CSR
    private String symptom; //Writen by CSR
    private boolean ifAssignedToDoctor; //Decided by CSR
    private boolean ifNeedHospitalized; //Decided by Doctor
    private Bed bedArrangement; //Assigned by Nurse
    private String situation; //Decided by Nurse to view the patient's recovery situation
    private boolean ifRecovered; //Assigned by Nurse
    private boolean ifGotMedicine; //Decided by medicine manager
    private static int patientCount = 1;

    public Patient() {
        this.patientID = patientCount;
        patientCount++;
        this.name = "";
        this.symptom = "";
        this.ifAssignedToDoctor = false; //Default value
        this.ifNeedHospitalized = false; //Default value
        this.bedArrangement = null;
        this.ifRecovered = false; //Default value
        this.ifGotMedicine = false; //Default value
        this.situation = "";
    }

    public int getPatientID() {
        return patientID;
    }

    public void setPatientID(int patientID) {
        this.patientID = patientID;
    }   

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }
    
    public boolean getIfNeedHospitalized() {
        return ifNeedHospitalized;
    }

    public void setIfNeedHospitalized(boolean ifNeedHospitalized) {
        this.ifNeedHospitalized = ifNeedHospitalized;
    }

    public Bed getBedArrangement() {
        return bedArrangement;
    }

    public void setBedArrangement(Bed bedArrangement) {
        this.bedArrangement = bedArrangement;
    }

    public String getSituation() {
        return situation;
    }

    public void setSituation(String situation) {
        this.situation = situation;
    }
    
    public boolean getIfRecovered() {
        return ifRecovered;
    }

    public void setIfRecovered(boolean ifRecovered) {
        this.ifRecovered = ifRecovered;
    }

    public boolean getIfAssignedToDoctor() {
        return ifAssignedToDoctor;
    }

    public void setIfAssignedToDoctor(boolean ifAssignedToDoctor) {
        this.ifAssignedToDoctor = ifAssignedToDoctor;
    }

    public boolean getIfGotMedicine() {
        return ifGotMedicine;
    }

    public void setIfGotMedicine(boolean ifGotMedicine) {
        this.ifGotMedicine = ifGotMedicine;
    }
    
    @Override
    public String toString(){
        return name;
    }

}
