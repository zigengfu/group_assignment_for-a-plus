/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Object;

/**
 *
 * @author yitongliu
 */
public class DiseaseSample {
    //A disease sample is created by doctor and sent to the sample manager in Research Center
    private String diseaseName;
    private String description;

    public DiseaseSample(String diseaseName, String description) {
        this.diseaseName = diseaseName;
        this.description = description;
    }

    public String getDiseaseName() {
        return diseaseName;
    }

    public void setDiseaseName(String diseaseName) {
        this.diseaseName = diseaseName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    
}
