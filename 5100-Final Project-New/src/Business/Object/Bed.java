/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Object;

/**
 *
 * @author fuzigeng
 */
public class Bed {
    private String bedNumber;
    private boolean availability;

    public Bed(String bedNumber, boolean availability) {
        this.bedNumber = bedNumber;
        this.availability = availability;
    }

    public String getBedNumber() {
        return bedNumber;
    }

    public void setBedNumber(String bedNumber) {
        this.bedNumber = bedNumber;
    }

    public boolean isAvailability() {
        return availability;
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }  
    
    @Override
    public String toString(){
        return this.getBedNumber();
    }
    
}
