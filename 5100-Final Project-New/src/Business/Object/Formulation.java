/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Object;

import Business.UserAccount.UserAccount;
import java.util.HashMap;

/**
 *
 * @author yitongliu
 */
public class Formulation {
    private String drugName;
    private String disease ;
    private UserAccount sender; 
    private UserAccount receiver;
    HashMap<String,Integer> materialStock = new HashMap<>();
    HashMap<String,Integer> materialQuantity = new HashMap<>();

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    } 

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public HashMap<String, Integer> getMaterialQuantity() {
        return materialQuantity;
    }

    public void setMaterialQuantity(HashMap<String, Integer> materialQuantity) {
        this.materialQuantity = materialQuantity;
    }  

    public UserAccount getSender() {
        return sender;
    }

    public void setSender(UserAccount sender) {
        this.sender = sender;
    }

    public UserAccount getReceiver() {
        return receiver;
    }

    public void setReceiver(UserAccount receiver) {
        this.receiver = receiver;
    }

    public HashMap<String, Integer> getMaterialStock() {
        return materialStock;
    }

    public void setMaterialStock(HashMap<String, Integer> materialStock) {
        this.materialStock = materialStock;
    }

}