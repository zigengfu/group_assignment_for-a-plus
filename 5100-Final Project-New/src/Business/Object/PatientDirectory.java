/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Object;

import java.util.ArrayList;

/**
 *
 * @author fuzigeng
 */
public class PatientDirectory {
    ArrayList<Patient> patientList;
    
    public PatientDirectory(){
        this.patientList = new ArrayList();
    }

    public ArrayList<Patient> getPatientList() {
        return patientList;
    }
    
    public Patient createPatient(String name, String symptom){
        Patient patient = new Patient();
        patient.setName(name);
        patient.setSymptom(symptom);
        this.patientList.add(patient);
        System.out.println("Create patient successfully!!!");
        return patient;
    }
    
}
