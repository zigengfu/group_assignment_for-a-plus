/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Object;

import java.util.ArrayList;

/**
 *
 * @author fuzigeng
 */
public class BedDirectory {
    private ArrayList<Bed> bedsList;

    public BedDirectory(){
        bedsList = new ArrayList<>();
        //Each hospital has 30 available beds at first
        for (int i = 0; i<30; i++){
            String bedNumber = "Bed "+ (i+1);
            Bed bed = new Bed(bedNumber, true);
            bedsList.add(bed);
        }
    }
    
    public void releaseBed(Bed releasedBed){
        for(Bed bed : this.bedsList){
            if(bed.getBedNumber().equals(releasedBed.getBedNumber()))
                bed.setAvailability(true);
        }
    }
    
    public void occupyBed(Bed selectedBed){
        for(Bed bed : this.bedsList){
            if(bed.getBedNumber().equals(selectedBed.getBedNumber()))
                bed.setAvailability(false);
        }
    }
    
    public ArrayList<Bed> getAvailableBeds() {
        ArrayList<Bed> availBeds = new ArrayList();
        
        for(Bed bed : this.bedsList){
            if(bed.isAvailability() == true)
                availBeds.add(bed);
        }
        return availBeds;
    }

    public ArrayList<Bed> getAllBeds() {
        return bedsList;
    }


    
}
